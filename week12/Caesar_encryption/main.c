#include <stdio.h>
#include <stdlib.h>
#include "CipherMethods.h"

#ifndef TRACE_ORG_DEC
#define TRACE_ORG_DEC



int main()
{
    char text[500];
    char passcode[9] = "ceng2002";
    char pass[9];
    printf("** You are about to enter a very secret cryptography service called CENG 2002 C-Secret Coded System **\nEnter your text:\n");
    scanf("%s", &text);
    system ("CLS");
    printf("In order to see the encrypted message, enter your passcode:\n");
    scanf("%s", &pass);

    for(;pass == passcode;){

        printf("Wrong passcode. Enter again\n");
        scanf("%s", &pass);

    }
    return 0;
}

#endif // TRACE_ORG_DEC
