#include <stdio.h>

int isPrime(int n, int m){

    if(m == 1)
        return 1;
    else{

        if(n%m == 0)
            return 0;
        else
            isPrime(n, m-1);

    }

}

int main(){

    int num;
    int prime;

    printf("Enter a number:");
    scanf("%d", &num);
    prime = isPrime(num, num/2);

    if(prime == 1){

        printf("It is a prime number.");

    } else{

        printf("It is not a prime number.");
    }

     return 0;


}
